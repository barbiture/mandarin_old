// slideUp / Down catalog group elements
$('.dropdown__toggle').click(function() {
  var $this = $(this).parent().find('.icon').parent(),
      content = $this.parent().next();
  if ( content.is(':hidden') ) {
    content.show('fast');
    $this.removeClass('hide');
  } else {
    content.slideUp('fast');
    $this.addClass('hide');
  }
})