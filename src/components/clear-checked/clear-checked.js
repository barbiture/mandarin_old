// checkup all input checked and reset value
$('.clear-checked').each(function() {
  var resetCheck = $(this).prev().find('.reset-checked'),
      checkBox = $(this).find('input:checkbox');
    resetCheck.click(function() {
      $(checkBox).prop('checked', false);
      resetCheck.hide();
    })

    if($(checkBox).is(':checked'))
      resetCheck.show();
    $(checkBox).change(function() {
      if(!$(checkBox).is(':checked'))
        resetCheck.hide();
      else
        resetCheck.show();
    })
})

