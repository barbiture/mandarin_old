'use strict';
//= ../../bower_components/jquery/dist/jquery.min.js
//= ../../bower_components/fancybox/dist/jquery.fancybox.min.js
$( document ).ready(function() {
  $('#filterToggle').click(function() {
    // $('.menu__shift').addClass('menu__shift_active');  // сдвигаем основной контент на право

    if ($('.filter').hasClass('filter_active')){
      $('.menu__shift').removeClass('menu__shift_active');
    }
    else{
      $('.menu__shift').addClass('menu__shift_active');
    }

    $('.menu__level').removeClass('menu__level_active');
    $('.menu__content').removeClass('menu__content_active');

    $('.filter').toggleClass('filter_active');  // добавляем класс для всего меню (ширина меню)
    $('.filter__content').toggleClass('filter__content_active');  // показываем контент првого уровня меню (ставим положение left = 0)
  })
  $('#menuToggleLevel').click(function() {
    
    if ($('.menu__level').hasClass('menu__level_active')){
      $('.menu__shift').removeClass('menu__shift_active');
    }
    else {
      $('.menu__shift').addClass('menu__shift_active');
    }
    $('.filter').removeClass('filter_active');
    $('.filter__content').removeClass('filter__content_active');
    $('.menu__level').toggleClass('menu__level_active');
    $('.menu__content').toggleClass('menu__content_active');
  })
  //= ../../components/dropdown/dropdown.js

  //= ../../components/menu__level/menu__level.js

});
//= ../../components/select/select.js









