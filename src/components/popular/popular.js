$(function() {
  var $owl = $('body').find('.owl-carousel');
  var carouselSettings = {
    nav: true,
    items: 5,
    loop: true,
    margin: 20,
    dots: false,
    navText: ['<svg class="icon icon-arrowLeft"><use xlink:href="#arrowLeft"</use></svg>', '<svg class="icon icon-arrowRight"><use xlink:href="#arrowRight"</use></svg>']
  }
  function initialize() {
    var containerWidth = $('body').find('.menu__shift').outerWidth();
    // console.log(containerWidth);
    if(containerWidth >= 1230) {
      // initialize owl-carousel if window screensize is less the 1230px
      $owl.owlCarousel( carouselSettings );

      // destroy jScrollPane
      $('.popular__slider').each(
        function(i) {
          // console.log(i);
          $(this).jScrollPane().data().jsp.destroy();
        })

      
      // console.log($('.popular__slider').each(function(){}));
    } else {
      // destroy owl-carousel and remove all depending classes if window screensize is bigger then 1230px
      $owl.trigger('destroy.owl.carousel').removeClass('owl-loaded');
      $owl.find('.owl-stage-outer').children().unwrap();
      // init jScrollPane
      $('.popular__slider').jScrollPane().data().jsp;
    }
  }
  // initilize after window resize
  var id;
  $(window).resize( function() {
    clearTimeout(id);
    id = setTimeout(initialize, 200);
  });

  // initilize onload
  initialize();
});