'use strict';

//= ../../bower_components/jquery/dist/jquery.min.js
//= ../../bower_components/fancybox/dist/jquery.fancybox.min.js
$( document ).ready(function() {
  $('#menuToggleLevel').click(function() {
    $('.menu__shift').toggleClass('menu__shift_active');  // сдвигаем основной контент на право
    $('.menu__level').toggleClass('menu__level_active');
    $('.menu__content').toggleClass('menu__content_active');
  })
  //= ../../components/catalog/catalog__favorite.js
  //= ../../components/catalog/catalog__compare.js
  //= ../../components/dropdown/dropdown.js
  //= ../../components/menu__level/menu__level.js
});
//= ../../components/select/select.js