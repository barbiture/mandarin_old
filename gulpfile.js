var browserSync    = require('browser-sync').create(),
    gulp           = require('gulp'),
    autoprefixer   = require('gulp-autoprefixer'),
    cheerio        = require('gulp-cheerio'),
    fileinclude    = require('gulp-file-include'),
    imagemin       = require('gulp-imagemin'),
    less           = require('gulp-less'),
    plumber        = require('gulp-plumber'),
    replace        = require('gulp-replace'),
    svgSprite      = require('gulp-svg-sprite'),
    rigger         = require('gulp-rigger'),
    svgmin         = require('gulp-svgmin'),
    watch          = require('gulp-watch'),
    jsmin          = require('gulp-jsmin'),
    rename          = require('gulp-rename'),
    sourcemaps     = require('gulp-sourcemaps');
// #PATH 

var prod = '../../barbiture.github.io/mandarin/',
    dev = './build/',
    //find all (dev+ & change base var
    base = dev;

var configSvgSymbol = {
  dest: './',
  log: 'info',
  dimension   : {     // Set maximum dimensions
    maxWidth  : 32,
    maxHeight : 32
  },
  shape : {
    spacing : {
      box: "padding",
      padding : 1
    },
  },
  mode : {
    symbol : {
      inline: true,
      dest: 'dest/symbol',
      sprite: 'sprite.svg',
      common: 'icons',
      render: {
        less: {
          template: 'src/assets/sprites/tmpl/symbol/_sprite.less',
          dest: 'sprite.less'
        }
      },
      example: {
        template: 'src/assets/sprites/tmpl/symbol/_sprite.html',
        dest: 'sprite.symbol.html'
      }
    }
  }
};
var configSvgCss = {
  dest: './',
  log: 'info',
  dimension   : {     // Set maximum dimensions
    maxWidth  : 32,
    maxHeight : 32
  },
  shape : {
    spacing : {
      box: "padding",
      padding : 0
    },
  },
  mode : {
    css: {
      dest: 'dest/css',
      sprite: 'sprite.svg',
      bust: false,
      common: 'icons',
      render: {
        less: {
          template: 'src/assets/sprites/tmpl/css/_sprite.less',
          dest: 'sprite.less'
        }
      },
      example: {
        dest: 'sprite.html'
      }
    }
  }
};
gulp.task('sprite', function(){
  gulp.src('./src/assets/sprites/dest/css/*.svg')
  .pipe(gulp.dest(dev+'/images'))
  .pipe(browserSync.stream());
});
gulp.task('svg',['sprite'], function () {
  gulp.src('./src/assets/sprites/svg/css/*.svg')
  // .pipe(svgmin())
  .pipe(svgSprite(configSvgCss))
  .pipe(gulp.dest('./src/assets/sprites'));
});

gulp.task('symbol', function () {

  // /Volumes/prod/barbiture.github.io/mandarin svg sprite
  return gulp.src('./src/assets/sprites/svg/symbol/*.svg')

  // remove all fill, style and stroke declarations in out shapes
  .pipe(cheerio({
    run: function ($) {
      // $('[fill]').removeAttr('fill');
      $('[stroke]').removeAttr('stroke');
      $('[style]').removeAttr('style');
    },
    parserOptions: {xmlMode: true}
  }))
  // cheerio plugin create unnecessary string '&gt;', so replace it.
  .pipe(replace('&gt;', '>'))
  .pipe(svgmin())
  .pipe(svgSprite(configSvgSymbol))
  .pipe(gulp.dest('./src/assets/sprites'));
});
gulp.task('sHtml', ['symbol', 'html', 'less']);

//HTML
gulp.task('html', function(){
  gulp.src('src/*.html')
    .pipe(fileinclude({
            prefix: '@@',
            basepath: '@file'
    }))
    .pipe(gulp.dest(dev+'/'))
    .pipe(browserSync.stream());
});

// less
gulp.task('less', function () {
  return gulp.src('src/assets/less/*.less')
    .pipe(sourcemaps.init())
    // .pipe(watchLess('src/assets/less/styles.less'))
    .pipe(less())
    // .pipe(autoprefixer({
    //      browsers: ['last 2 versions'],
    //      cascade: false
    //  }))
    .pipe(plumber.stop())
    .pipe(sourcemaps.write('maps'))
    .pipe(gulp.dest(dev+'/styles'))
    .pipe(browserSync.stream());
});
// gulp.task('test', function() {
//   console.log('msg');
// });
// IMG
gulp.task('img', function(){
  gulp.src('src/assets/img/**/*')
    .pipe(gulp.dest(dev+'/images/'))
    .pipe(imagemin())
    .pipe(browserSync.stream());
});

// js
gulp.task('js', function(){
  gulp.src('src/assets/js/*.js')
    .pipe(rigger())
    // .pipe(plumber())
    // .pipe(jsmin())
    // .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest(dev+'/js'))
    .pipe(browserSync.stream());
});

// fonts
gulp.task('fonts',['fontsCss'], function(){
  gulp.src('src/fonts/Tahoma/fonts/**/')
    .pipe(gulp.dest(dev+'/fonts/'))
    .pipe(browserSync.stream());
});
// fonts
gulp.task('fontsCss', function(){
  gulp.src('src/fonts/Tahoma/Tahoma.css')
    .pipe(gulp.dest(dev+'/styles/'))
    .pipe(browserSync.stream());
});

// nouislider
gulp.task('nouislider', function(){
  gulp.src('./src/bower_components/nouislider/src/*.less')
  .pipe(gulp.dest('./src/components/nouislider/'))
  .pipe(browserSync.stream());
});

//+----------------------------------------------------------------+//

gulp.task('build', ['img', 'sprite', 'symbol', 'fonts', 'less', 'js', 'html']);

gulp.task('default', [base, 'watch', 'serve']);

gulp.task('serve', ['watch'], function (){
  browserSync.init({
    server: {
      baseDir: base
    },
    host: "localhost",
    port: 8080,
    tunnel: false,
    directory: true,
    browser: "/Applications/Google Chrome.app"
  });
});

//+----------------------------------------------------------------+//

gulp.task('watch', function(){
  gulp.watch(['src/**/*.html', 'src/components/**/*.html'], ['html']);
  gulp.watch(['src/assets/less/**/*.less', 'src/components/**/*.less'], ['less']);
  gulp.watch(['src/assets/js/*.js', 'src/components/**/*.js'], ['js']);
  gulp.watch('src/assets/img/**/*.*', ['img']);
  gulp.watch('src/assets/fonts/**/*.*', ['fonts']);
});