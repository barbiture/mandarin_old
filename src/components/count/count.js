$(function () {
  var inc = $('.plus');
  var dec = $('.minus');
  inc.click(function() {
    var input = $(this).parent().find('input');
    var num =+ input.val()+1;
    input.val(num);
  })
  dec.click(function() {
    var input = $(this).parent().find('input');
    if (input.val() > 0) {
      var num =+ input.val()-1;
      input.val(num);
    }
  })
});