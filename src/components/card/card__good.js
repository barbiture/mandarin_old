//= ../catalog/catalog__favorite.js
//= ../catalog/catalog__compare.js
$(function() {
  var toggleClick = $('.toggle__click');
  function initialize() {

    var containerWidth = $('body').find('.menu__shift').outerWidth();

    if (containerWidth >= 1230) {
      
      $('.card__good__slide img').addClass('zoom');
      $('.slick-active .zoom').imagezoomsl({
        zoomrange: [3, 3],
        magnifiersize: [411, 541],
        rightoffset: 80
      });
      toggleClick.addClass('toggle__click_active');
      toggleClick.next('.toggle__content').show();
    }
    else {
      $('.zoom').removeClass('zoom');
      $('.magnifier, .cursorshade, .statusdiv, .tracker').remove();
      toggleClick.click(function() {
        $(this).toggleClass('toggle__click_active');
        $(this).next('.toggle__content').toggle();
      });
    }
  }
  $(window).resize( function() {
    initialize();
  });

  // initilize onload
  initialize();

  var $slickSlide = $('body').find('.card__good__slide');
  var slickSlideSettings = {
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.card__good__nav',
  }
  $slickSlide.slick(slickSlideSettings)
    .on('afterChange', function() {
      $('.slick-active .zoom').imagezoomsl({
        zoomrange: [3, 3],
        magnifiersize: [411, 541],
        cursorshadeborder: '1px solid #c0c0c0',
        rightoffset: 80
      })
    })
    .on('beforeChange', function() {
      $('.magnifier, .cursorshade, .statusdiv, .tracker').remove();
    });

   $('.card__good__nav').slick({
     slidesToShow: 4,
     slidesToScroll: 1,
     asNavFor: '.card__good__slide',
     vertical: true,
     centerMode: false,
     focusOnSelect: true,
     prevArrow: '<svg class="icon icon-arrowTop"><use xlink:href="#arrowTop"></use></svg>',
     nextArrow: '<svg class="icon icon-arrowBottom"><use xlink:href="#arrowBottom"></use></svg>',
     responsive: [{
       breakpoint: 1230,
       settings: {
         vertical: false,
         prevArrow: '<svg class="icon icon-arrowLeft"><use xlink:href="#arrowLeft"></use></svg>',
         nextArrow: '<svg class="icon icon-arrowRight"><use xlink:href="#arrowRight"></use></svg>',
       }
     }]
   });
   $('.slick-active .zoom').imagezoomsl({
     zoomrange: [3, 3],
     magnifiersize: [411, 541],
     cursorshadeborder: '1px solid #c0c0c0',
     rightoffset: 80
   });
});
